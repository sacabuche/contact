package contact

import (
	"errors"
	"fmt"
	"net/smtp"
	"strconv"
	"strings"
)

type Message struct {
	Name    string
	Email   string
	Subject string
	Body    string
}

type Account struct {
	Username  string
	Password  string
	SMTPHost  string
	Port      int
	FromEmail string
	ToEmails  []string
}

type Header struct {
	Name  string
	Value string
}

const HEADERS_SPACE = "\r\n\r\n"

func trimAndCount(s string) int {
	return len(strings.TrimSpace(s))
}

func containsAll(s string, sub string) bool {
	counter := len(sub)
	for _, v := range sub {
		if strings.ContainsRune(s, v) {
			counter--
		}
		if counter <= 0 {
			return true
		}
	}
	return false
}

func (m Message) Validate() error {
	if trimAndCount(m.Body) == 0 {
		return errors.New("Missing Body")
	}
	if !containsAll(m.Email, "@.") {
		return errors.New("Not An Email")
	}
	return nil
}

func (h Header) String() (string, error) {
	if strings.ContainsAny(h.Value, "\n\r") {
		return "", fmt.Errorf("Header Insertion on: %s", h.Name)
	}

	value := strings.Trim(h.Value, " <>")
	if len(value) == 0 {
		return "", fmt.Errorf("Missing Value on Header: %s", h.Name)
	}
	return fmt.Sprintf("%s: %s", h.Name, h.Value), nil
}

func SendMail(message *Message, account *Account) error {
	err := message.Validate()
	if err != nil {
		return err
	}

	headers := []Header{
		{"To", strings.Join(account.ToEmails, ", ")},
		{"Reply-To", message.Email},
		{"Subject", message.Subject},
	}

	auth := smtp.PlainAuth(
		"",
		account.Username,
		account.Password,
		account.SMTPHost,
	)

	if len(strings.TrimSpace(message.Name)) != 0 {
		message.Body = fmt.Sprintf("Mensaje De: %s\n\n%s", message.Name, message.Body)
	}

	string_headers := make([]string, len(headers))
	for i, h := range headers {
		value, err := h.String()
		if err != nil {
			return err
		}
		string_headers[i] = value
	}
	fmt.Println(string_headers)

	content := strings.Join(string_headers, "\r\n") + HEADERS_SPACE + message.Body
	host := account.SMTPHost + ":" + strconv.Itoa(account.Port)
	err = smtp.SendMail(
		host,
		auth,
		account.FromEmail,
		account.ToEmails,
		[]byte(content))

	return err
}
